-- Step 1.1 Staging Tables (all done)
-- Step 1.2: Dimension Table (all done)



-- Step 1.3: Fact Table
CREATE TABLE FactSupplierPurchases (
    PurchaseID SERIAL PRIMARY KEY,
    SupplierID INT,
    TotalPurchaseAmount DECIMAL,
    PurchaseDate DATE,
    NumberOfProducts INT,
    FOREIGN KEY (SupplierID) REFERENCES DimSupplier(SupplierID)
);



-- Step 1.4: Populating the Fact Table
INSERT INTO FactSupplierPurchases (SupplierID, TotalPurchaseAmount, PurchaseDate, NumberOfProducts)
SELECT 
    p.SupplierID, 
    SUM(od.UnitPrice * od.Quantity) AS TotalPurchaseAmount, 
    CURRENT_DATE AS PurchaseDate,
    COUNT(DISTINCT od.ProductID) AS NumberOfProducts
FROM staging_order_details od
JOIN staging_products p ON od.ProductID = p.ProductID
GROUP BY p.SupplierID;



-- Step 1.5: Querying the Data Mart

-- Supplier Performance Report (not work)
-- SELECT
--     s.CompanyName,
--     AVG(fsp.DeliveryLeadTime) AS AverageLeadTime,
--     SUM(fsp.OrderAccuracy) / COUNT(fsp.PurchaseID) AS AverageOrderAccuracy,
--     COUNT(fsp.PurchaseID) AS TotalOrders
-- FROM FactSupplierPurchases fsp
-- JOIN DimSupplier s ON fsp.SupplierID = s.SupplierID
-- GROUP BY s.CompanyName
-- ORDER BY AverageLeadTime, AverageOrderAccuracy DESC;



-- Supplier Spending Analysis
SELECT
    s.CompanyName,
    SUM(fsp.TotalPurchaseAmount) AS TotalSpend,
    EXTRACT(YEAR FROM fsp.PurchaseDate) AS Year,
    EXTRACT(MONTH FROM fsp.PurchaseDate) AS Month
FROM FactSupplierPurchases fsp
JOIN DimSupplier s ON fsp.SupplierID = s.SupplierID
GROUP BY s.CompanyName, Year, Month
ORDER BY TotalSpend DESC;



-- Product Cost Breakdown by Supplier
SELECT
    s.CompanyName,
    p.ProductName,
    AVG(od.UnitPrice) AS AverageUnitPrice,
    SUM(od.Quantity) AS TotalQuantityPurchased,
    SUM(od.UnitPrice * od.Quantity) AS TotalSpend
FROM staging_order_details od
JOIN staging_products p ON od.ProductID = p.ProductID
JOIN DimSupplier s ON p.SupplierID = s.SupplierID
GROUP BY s.CompanyName, p.ProductName
ORDER BY s.CompanyName, TotalSpend DESC;



-- Supplier Reliability Score Report (not work)
-- SELECT
--     s.CompanyName,
--     (COUNT(fsp.PurchaseID) FILTER (WHERE fsp.OnTimeDelivery = TRUE) / COUNT(fsp.PurchaseID)::FLOAT) * 100 AS ReliabilityScore
-- FROM FactSupplierPurchases fsp
-- JOIN DimSupplier s ON fsp.SupplierID = s.SupplierID
-- GROUP BY s.CompanyName
-- ORDER BY ReliabilityScore DESC;



-- Top Five Products by Total Purchases per Supplier
SELECT
    s.CompanyName,
    p.ProductName,
    SUM(od.UnitPrice * od.Quantity) AS TotalSpend
FROM staging_order_details od
JOIN staging_products p ON od.ProductID = p.ProductID
JOIN DimSupplier s ON p.SupplierID = s.SupplierID
GROUP BY s.CompanyName, p.ProductName
ORDER BY s.CompanyName, TotalSpend DESC
LIMIT 5;




-- Step 2.1 Staging Tables (all done)
-- Step 2.2: Dimension Table (all done)



-- Step 2.3: Fact Table
CREATE TABLE FactProductSales (
    FactSalesID SERIAL PRIMARY KEY,
    DateID INT,
    ProductID INT,
    QuantitySold INT,
    TotalSales DECIMAL(10,2),
    FOREIGN KEY (DateID) REFERENCES DimDate(DateID),
    FOREIGN KEY (ProductID) REFERENCES DimProduct(ProductID)
);



-- Step 2.4: Populating the Fact Table (done)
INSERT INTO FactProductSales (DateID, ProductID, QuantitySold, TotalSales)
SELECT 
    (SELECT DateID FROM DimDate WHERE Date = s.OrderDate) AS DateID,
    p.ProductID, 
    sod.Quantity, 
    (sod.Quantity * sod.UnitPrice) AS TotalSales
FROM staging_order_details sod
JOIN staging_orders s ON sod.OrderID = s.OrderID
JOIN staging_products p ON sod.ProductID = p.ProductID;



-- Step 2.5: Querying the Data Mart

-- Top-Selling Products
SELECT 
    p.ProductName,
    SUM(fps.QuantitySold) AS TotalQuantitySold,
    SUM(fps.TotalSales) AS TotalRevenue
FROM 
    FactProductSales fps
JOIN DimProduct p ON fps.ProductID = p.ProductID
GROUP BY p.ProductName
ORDER BY TotalRevenue DESC
LIMIT 5;

-- Products Below Reorder (not work)
-- SELECT 
--     p.ProductName, 
--     p.UnitsInStock, 
--     p.reorderlevel
-- FROM 
--     DimProduct p
-- WHERE 
--     p.UnitsInStock < p.ReorderLevel;



-- Sales Trends by Product Category:
SELECT 
    c.CategoryName, 
    EXTRACT(YEAR FROM d.Date) AS Year,
    EXTRACT(MONTH FROM d.Date) AS Month,
    SUM(fps.QuantitySold) AS TotalQuantitySold,
    SUM(fps.TotalSales) AS TotalRevenue
FROM 
    FactProductSales fps
JOIN DimProduct p ON fps.ProductID = p.ProductID
JOIN DimCategory c ON p.CategoryID = c.CategoryID
JOIN DimDate d ON fps.DateID = d.DateID
GROUP BY c.CategoryName, EXTRACT(YEAR FROM d.Date), EXTRACT(MONTH FROM d.Date)
ORDER BY Year, Month, TotalRevenue DESC;



-- Inventory Valuation
SELECT 
    p.ProductName,
    p.UnitsInStock,
    p.UnitPrice,
    (p.UnitsInStock * p.UnitPrice) AS InventoryValue
FROM 
    DimProduct p
ORDER BY InventoryValue DESC;
	


-- Supplier Performance Based on Product Sales
SELECT 
    s.CompanyName,
    COUNT(DISTINCT fps.FactSalesID) AS NumberOfSalesTransactions,
    SUM(fps.QuantitySold) AS TotalProductsSold,
    SUM(fps.TotalSales) AS TotalRevenueGenerated
FROM 
    FactProductSales fps
JOIN DimProduct p ON fps.ProductID = p.ProductID
JOIN DimSupplier s ON p.SupplierID = s.SupplierID
GROUP BY s.CompanyName
ORDER BY TotalRevenueGenerated DESC;
	


-- Step 3.1 Staging Tables (all done)
-- Step 3.2: Dimension Table (all done)

-- Step 3.3: Fact Table, Step 3.4: Populating the Fact Table (all tables were created earlier)



-- Step 3.5: Querying the Data Mart

-- Customer Sales Overview
SELECT cu.CompanyName, SUM(fs.TotalAmount) AS TotalSpent, COUNT(DISTINCT fs.salesid) AS TransactionsCount
FROM FactSales fs
JOIN DimCustomer cu ON fs.CustomerID = cu.CustomerID
GROUP BY cu.CompanyName
ORDER BY TotalSpent DESC;



-- Top Five Customers by Total Sales
SELECT 
    c.CompanyName,
    SUM(fs.TotalAmount) AS TotalSpent
FROM 
    FactSales fs
JOIN DimCustomer c ON fs.CustomerID = c.CustomerID
GROUP BY c.CompanyName
ORDER BY TotalSpent DESC
LIMIT 5;



-- Customers by Region
SELECT 
    c.Region,
    COUNT(*) AS NumberOfCustomers,
    SUM(fs.TotalAmount) AS TotalSpentInRegion
FROM 
    FactSales fs
JOIN DimCustomer c ON fs.CustomerID = c.CustomerID
GROUP BY c.Region
ORDER BY NumberOfCustomers DESC;



-- Customer Segmentation Analysis
SELECT 
    c.CustomerID, 
    c.CompanyName,
    CASE
        WHEN SUM(fcs.TotalAmount) > 10000 THEN 'VIP'
        WHEN SUM(fcs.TotalAmount) BETWEEN 5000 AND 10000 THEN 'Premium'
        ELSE 'Standard'
    END AS CustomerSegment
FROM 
    FactCustomerSales fcs
JOIN DimCustomer c ON fcs.CustomerID = c.CustomerID
GROUP BY c.CustomerID, c.CompanyName
ORDER BY SUM(fcs.TotalAmount) DESC;



-- Step 4.1 Staging Tables, Step 4.2: Dimension Table, Step 4.3: Fact Table, Step 4.4: Populating the Fact Table (all done)



-- Step 4.5: Querying the Data Mart
--Aggregate Sales by Month and Category

SELECT d.Month, d.Year, c.CategoryName, SUM(fs.TotalAmount) AS TotalSales
FROM FactSales fs
JOIN DimDate d ON fs.DateID = d.DateID
JOIN DimCategory c ON fs.CategoryID = c.CategoryID
GROUP BY d.Month, d.Year, c.CategoryName
ORDER BY d.Year, d.Month, TotalSales DESC;	
	

-- Top-Selling Products per Quarter
SELECT d.Quarter, d.Year, p.ProductName, SUM(fs.QuantitySold) AS TotalQuantitySold
FROM FactSales fs
JOIN DimDate d ON fs.DateID = d.DateID
JOIN DimProduct p ON fs.ProductID = p.ProductID
GROUP BY d.Quarter, d.Year, p.ProductName
ORDER BY d.Year, d.Quarter, TotalQuantitySold DESC
LIMIT 5;
				

			
--Sales Performance by Employee	
SELECT e.FirstName, e.LastName, COUNT(fs.SalesId) AS NumberOfSales, SUM(fs.TotalAmount) AS TotalSales
FROM FactSales fs
JOIN DimEmployee e ON fs.EmployeeID = e.EmployeeID
GROUP BY e.FirstName, e.LastName
ORDER BY TotalSales DESC;	



-- Customer Sales Overview
SELECT cu.CompanyName, SUM(fs.TotalAmount) AS TotalSpent, COUNT(DISTINCT fs.SalesID) AS TransactionsCount
FROM FactSales fs
JOIN DimCustomer cu ON fs.CustomerID = cu.CustomerID
GROUP BY cu.CompanyName
ORDER BY TotalSpent DESC;



--Monthly Sales Growth Rate	
WITH MonthlySales AS (
    SELECT
        d.Year,
        d.Month,
        SUM(fs.TotalAmount) AS TotalSales
    FROM FactSales fs
    JOIN DimDate d ON fs.DateID = d.DateID
    GROUP BY d.Year, d.Month
),
MonthlyGrowth AS (
    SELECT
        Year,
        Month,
        TotalSales,
        LAG(TotalSales) OVER (ORDER BY Year, Month) AS PreviousMonthSales,
        (TotalSales - LAG(TotalSales) OVER (ORDER BY Year, Month)) / LAG(TotalSales) OVER (ORDER BY Year, Month) AS GrowthRate
    FROM MonthlySales
)
SELECT * FROM MonthlyGrowth;
